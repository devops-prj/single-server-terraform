variable "ssh_key_name" {
  type = string
  description = "SSH key for remote access"
}

variable "aws_availability_zone" {
  type = string
  description = "AWS Availability Zone"
}

variable "server_name" {
  type = string
  description = "Server name"
}

variable "instance_type" {
  type = string
  description = "AWS instance type"
  default = "t2.medium"
}

variable "vpc_id" {
  type = string
  description = "AWS VPC id"
}

variable "allowed_cidr_blocks" {
  type = list(string)
  description = "AWS Network CIDR blocks"
  default = ["0.0.0.0/0"]
}

variable "ssh_port" {
  type = number
  description = "The port the EC2 Instance should listen on for SSH requests."
  default     = 22
}

variable "ssh_user" {
  type = string
  description = "SSH user name to use for remote exec connections,"
  default     = "ubuntu"
}

variable "private_key_path" {
  type = string
  description = "Path to private key"
}

variable "use_volume" {
  type = string
  description = "Flag indicating if server should mount an EBS volume or not"
  default = "false"
}

variable "volume_name" {
  type = string
  description = "EBS volume name"
  default = "server_data"
}

variable "volume_type" {
  type = string
  description = "EBS volume type"
  default = "gp2"
}

variable "volume_size" {
  type = number
  description = "EBS volume size"
  default = 10
}

variable "skip_destroy_volume" {
  type = bool
  description = "If set to true will not destroy EBS volume with the other resources"
  default = false
}

variable "mount_ebs_script_path" {
  type = string
  description = "Path to mount-ebs.sh script"
}