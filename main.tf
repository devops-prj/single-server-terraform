terraform {
  required_version = ">= 0.12"
}

resource "aws_instance" "server" {
  ami             = data.aws_ami.latest-server.id
  instance_type   = var.instance_type

  associate_public_ip_address = true
  availability_zone = var.aws_availability_zone

  security_groups = [aws_security_group.server_security_group.name]

  tags = {
    Name = var.server_name
  }

  key_name = var.ssh_key_name
}

resource "aws_ebs_volume" "volume_data" {
  type = var.volume_type
  size = var.volume_size
  availability_zone = var.aws_availability_zone

  tags = {
    Name = var.volume_name
  }

  count = var.use_volume == "true" ? 1 : 0
}

resource "aws_volume_attachment" "ebs_att" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.volume_data[0].id
  instance_id = aws_instance.server.id

  force_detach = !var.skip_destroy_volume
  skip_destroy = var.skip_destroy_volume

  count = var.use_volume == "true" ? 1 : 0
}

data "aws_vpc" selected-vpc {
  id = var.vpc_id
}

resource "null_resource" "generic_provisioner" {
  triggers = {
    public_ip = aws_instance.server.public_ip
  }

  depends_on = [aws_volume_attachment.ebs_att]

  connection {
    type = "ssh"
    host = aws_instance.server.public_ip
    user = var.ssh_user
    port = var.ssh_port
    agent = true
    private_key = var.private_key_path
  }

  // copy init script to the server
  provisioner "file" {
    source      = var.mount_ebs_script_path
    destination = "/tmp/mount-ebs.sh"
  }

  // change permissions to executable and execute
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/mount-ebs.sh",
      "/tmp/mount-ebs.sh",
    ]
  }

  count = var.use_volume == "true" ? 1 : 0
}

# ---------------------------------------------------------------------------------------------------------------------
# CREATE A SECURITY GROUP TO CONTROL WHAT REQUESTS CAN GO IN AND OUT OF EACH EC2 INSTANCE
# ---------------------------------------------------------------------------------------------------------------------

resource "aws_security_group" "server_security_group" {
  description = "Security group for the server ${var.server_name}"
  vpc_id      = var.vpc_id

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Name = "${var.server_name} security group"
  }
}
