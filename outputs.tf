output "public_instance_id" {
  value = aws_instance.server.id
}

output "public_instance_dns" {
  value = aws_instance.server.public_dns
}

output "public_instance_ip" {
  value = aws_instance.server.public_ip
}

output "security_group_id" {
  value = aws_security_group.server_security_group.id
}