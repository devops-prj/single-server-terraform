#!/bin/bash

if $(sudo file -s /dev/xvdh | grep -q "/dev/xvdh: data"); then
    echo "Empty EBS"
    sudo mkfs -t ext4 /dev/xvdh
else
    echo "Non empty EBS"
fi

# create mount point
sudo mkdir /mnt/ebs

# mount ebs
sudo mount -t auto -v /dev/xvdh /mnt/ebs

# edit fstab to mount automatically between restarts
UUID=$(sudo blkid -s UUID -o value /dev/xvdh)

echo "UUID=$UUID  /mnt/ebs  ext4  defaults,nofail  0  2" | sudo tee -a /etc/fstab